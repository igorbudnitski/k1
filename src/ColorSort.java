public class ColorSort {

   enum Color {red, green, blue};

   public static void main(String[] param) {
      Color[] balls = generateArray(10); // calling generateArray method
      System.out.println("Before sort: ");
      printArray(balls);
      System.out.println("After sort: ");
      reorder(balls);
      printArray(balls);
   }

   public static void reorder(Color[] balls) {
      int red = 0;
      int blue = 0;
      int green = 0;
      for (int i = 0; i < balls.length; i++){
         switch (balls[i]){
            case red:
               red++;
               break;
            case green:
               green++;
               break;
            case blue:
               blue++;
               break;
            default:
               break;
         }
      }
      System.out.println("red = " + red + " green = " + green + " blue = " + blue); // just for check
      int location = 0;
         for (int i = 0; i < red; i++, location ++){
            balls[location] = Color.red;
         }
         for (int i = 0; i < green; i++, location ++){
            balls[location] = Color.green;
         }
         for (int i = 0; i < blue; i++, location ++){
         balls[location] = Color.blue;
         }
   }

   private static Color[] generateArray(int i) {
      Color[]balls= new Color[i];
      for (int l=0; l < balls.length; l++) {
         double rnd = Math.random();
         if (rnd < 1./3.) {
            balls[l] = Color.red;
         } else  if (rnd > 2./3.) {
            balls[l] = Color.blue;
         } else {
            balls[l] = Color.green;
         }
      }
      return balls;
   }

      // creating method to print the array with for each loop
      private static void printArray(Color[] balls) {
         for (Color t : balls) {
            System.out.println(t);
         }
         System.out.println();
      }
}